package Hacenda;

/**
 * La classe principal dels projecte.
 * @author Carlos Urbina
 * @version 1.0
 */
public class Gos {
	
	private int edat;
	private String nom;
	private int fills;
	private char sexe;
	private Raça raçaGos;
	private GosEstat estat = GosEstat.VIU;
	public static int gossosCreats = 0;
	
	//CONSTRUCTORS
	
	/**
	 * Constructor per defecte, no rep cap paràmetre.
	 */
	public Gos() {
		this.edat = 4;
		this.nom = "SenseNom";
		this.fills = 0;
		this.sexe = 'M';
		quantitatGossos();
	}
	
	/**
	 * Constructor per posar els atributs manualment rep diferents atributs.
	 * @param e rep un valor enter.
	 * @param n rep un valor String.
	 * @param f rep un valor enter.
	 * @param s rep un valor char.
	 */
	public Gos(int e, String n, int f, char s) {
		this.edat = e;
		this.nom = n;
		this.fills = f;
		this.sexe = s;
		quantitatGossos();
	}
	
	/**
	 * Constructor que copia els atributs d'un altre objecte que rep per paràmetre a l'objecte que l'ha cridat.
	 * @param c rep un valor tipus Gos.
	 */
	public Gos(Gos c) {
		this.edat = c.edat;
		this.nom = c.nom;
		this.fills = c.fills;
		this.sexe = c.sexe;
		quantitatGossos();
	}
	
	/**
	 * Constructor que rep un string que hi serà el nom, després posà tots els altres atributs per defecte.
	 * @param n rep un String.
	 */
	public Gos(String n) {
		this.edat = 4;
		this.nom = n;
		this.fills = 0;
		this.sexe = 'M';
		quantitatGossos();
	}
	
	/**
	 * Constructor que incorpora la raça com paràmetre i truca a un altre constructor.
	 * @param n rep un valor String.
	 * @param m rep un valor GosMida.
	 * @param t rep un valor enter.
	 * @param e rep un valor enter.
	 * @param nR rep un valor String.
	 * @param f rep un valor enter.
	 * @param s rep un valor char.
	 */
	public Gos(String n, GosMida m, int t, int e, String nR, int f, char s) {
		this (e, nR, f, s);
		this.raçaGos = new Raça(n, m, t);
	}
	
	//ACCESSORS
	
	/**
	 * Modifica l'atribut edat, de l'objecte. També veu si el temps de vida de la seva raça es més petit que que la edat modifica el seu estat.
	 * @param e rep un enter.
	 */
	public void setEdat(int e) {
		if (this.raçaGos != null && e > this.raçaGos.getTempsVida()) {
			this.estat = GosEstat.MORT;
			
		}else if (this.raçaGos == null && e >= 10) {
			this.estat = GosEstat.MORT;
		}
		this.edat = e;
	}
	
	/**
	 * Retorna l'atribut edat, de l'objecte.
	 * @return edat retorna un valor enter.
	 */
	public int getEdat() {
		return this.edat;
	}
	
	/**
	 * Modifica l'atribut nom, de l'objecte.
	 * @param n rep un valor enter.
	 */
	public void setNom(String n) {
		this.nom = n;
	}
	
	/**
	 * Retorna l'atribut nom, de l'objecte.
	 * @return nom retorna un valor String.
	 */
	public String getNom() {
		return this.nom;
	}
	
	/**
	 * Modifica l'atribut fills, de l'objecte
	 * @param f rep un valor enter.
	 */
	public void setFills(int f) {
		this.fills = f;
	}
	
	/**
	 * Retorna l'atribut fills, de l'objecte.
	 * @return fills retorna un valor enter.
	 */
	public int getFills() {
		return this.fills;
	}
	
	/**
	 * Modifica l'atribut sexe, de l'objecte.
	 * @param s rep un valor char.
	 */
	public void setSexe(char s) {
		this.sexe = s;
	}
	
	/**
	 * Retorna l'atribut sexe, de l'objecte
	 * @return sexe retorna un valor char.
	 */
	public char getSexe() {
		return this.sexe;
	}
	
	/**
	 * Modifica els atributs de l'objecte raçaGos.
	 * @param n rep un valor String.
	 * @param m rep un valor GosMida.
	 * @param t rep un valor enter.
	 * @param d rep un valor boolean.
	 */
	public void setRaçaGos(String n, GosMida m, int t, boolean d) {
		this.raçaGos.setNomRaça(n);
		this.raçaGos.setMida(m);
		this.raçaGos.setTempsVida(t);
		this.raçaGos.setDominant(d);
	}
	
	/**
	 * Retorna l'atribut objecte raçaGos.
	 * @return raçaGos retorrna un objecte.
	 */
	public Raça getRaçaGos() {
		return this.raçaGos;
	}
	
	/**
	 * Modifica l'atribut enum estat.
	 * @param g rep un valor GosEstat.
	 */
	public void setEstat(GosEstat g) {
		this.estat = g;
	}
	
	/**
	 * Retorna l'atribut enum estat.
	 * @return estat retorna un objecte enum.
	 */
	public GosEstat getEstat() {
		return this.estat;
	}
	
	/**
	 * Modifica tots els atributs de l'objecte.
	 * @param e rep un enter.
	 * @param n rep un string.
	 * @param f rep un enter.
	 * @param s rep un char.
	 */
	public void setGos(int e, String n, int f, char s) {
		this.edat = e;
		this.nom = n;
		this.fills = f;
		this.sexe = s;
	}
	
	//OPERADORS
	
	/**
	 * Funció que mostra per pantalla un missatge de "guau guau".
	 */
	public static void borda() {
		System.out.println("guau guau");
	}
	
	/**
	 * Funció que mostra per pantalla les propietats de l'objecte que la crida.
	 */
	public void visualitzar() {
		System.out.println("\nEl gos té: " + this.getEdat() + " anys");
		System.out.println("El nom del gos és: " + this.getNom());
		System.out.println("El numero de fills que té el gos és de: " + this.getFills());
		
		if (this.getSexe() == 'M') {
			System.out.println("El gos és de sexe: " + this.getSexe() + " (Mascle)");
			
		}else if (this.getSexe() == 'F') {
			System.out.println("El gos és de sexe: " + this.getSexe() + " (Famella)");
			
		}else {
			System.out.println("El sexe del gos no està bé definit");
		}
	}
	
	/**
	 * Funció que retorna les propietats de l'objecte que la crida.
	 */
	public String toString() {
		if (this.raçaGos == null) {
			return "El nom del gos és " + this.getNom() + ", té " + this.getEdat() + " anys, és de sexe " + this.getSexe() + " i té " + this.getFills() + " fills. \nEl gos no té raça definida y l'estat del gos és " + this.getEstat() + ".\n";
			
		}else {
			return "El nom del gos és " + this.getNom() + ", té " + this.getEdat() + " anys, és de sexe " + this.getSexe() + " i té " + this.getFills() + " fills, \n" + this.raçaGos.toString() + ".\nL'estat del gos és " + this.getEstat() + ".\n";
			
		}

	}
	
	/**
	 * Funció que rep un objecte i copia els atributs d'aquest a l'objecte que li ha trucat.
	 * @param c rep un valor tipus Gos.
	 */
	public void clonar(Gos c) {
		this.edat = c.edat;
		this.nom = c.nom;
		this.fills = c.fills;
		this.sexe = c.sexe;
	}
	
	/**
	 * Funció per sumar la variable gossosCreats.
	 */
	public static void quantitatGossos() {
		gossosCreats++;
		System.out.println("\nObjecte nou creat\n");
	}
	
	public Gos aparellar(Gos g) {
		Gos femella = null;
		Gos mascle = null;
		Gos fill = null;
		
		if (this.getSexe() == 'F' && g.getSexe() == 'M') {
			femella = this;
			mascle = g;
		
		}else if (this.getSexe() == 'M' && g.getSexe() == 'F') {
			femella = g;
			mascle = this;
		}
		
		if (this.getEdat() >= 2 && this.getEdat() <= 10 && g.getEdat() >= 2 && g.getEdat() <= 10) {
			if (femella != null && mascle != null) {
				if (femella.getEstat() == GosEstat.VIU && mascle.getEstat() == GosEstat.VIU) {
					if (femella.getFills() < 3) {
						if (mascle.raçaGos.getMida() == GosMida.PETIT || mascle.raçaGos.getMida() == GosMida.MITJA && femella.raçaGos.getMida() != GosMida.PETIT || mascle.raçaGos.getMida() == GosMida.GRAN && femella.raçaGos.getMida() == GosMida.GRAN) {
							char[] sexe = {'M', 'F'};
							char aleatori = sexe[(int)(Math.random() * 2)];
				
							if (mascle.raçaGos.getDominant() && !femella.raçaGos.getDominant()) {
								if (aleatori == 'M') {
									fill = new Gos(mascle.raçaGos.getNomRaça(), GosMida.PETIT, femella.raçaGos.getTempsVida(), 0, "Fill de " + mascle.getNom(), 0, aleatori);
								}
								if (aleatori == 'F') {
									fill = new Gos(mascle.raçaGos.getNomRaça(), GosMida.PETIT, femella.raçaGos.getTempsVida(), 0, "Fill de " + femella.getNom(), 0, aleatori);
								}
								
							}else {
								if (aleatori == 'M') {
									fill = new Gos(femella.raçaGos.getNomRaça(), GosMida.PETIT, femella.raçaGos.getTempsVida(), 0, "Fill de " + mascle.getNom(), 0, aleatori);
								}
								if (aleatori == 'F') {
									fill = new Gos(femella.raçaGos.getNomRaça(), GosMida.PETIT, femella.raçaGos.getTempsVida(), 0, "Fill de " + femella.getNom(), 0, aleatori);
								}
							}
						}
					}
				}	
			}
		}
		
		return fill;
	}
}
