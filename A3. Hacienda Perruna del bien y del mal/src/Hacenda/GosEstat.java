package Hacenda;

/**
 * Classe enum per posar l'estat del gos.
 * @author Carlos Urbina
 *
 */
public enum GosEstat {
	VIU,
	MORT
}
